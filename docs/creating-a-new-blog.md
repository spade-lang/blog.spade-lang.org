Consider how Rust has a split [main blog](https://blog.rust-lang.org/) and [the
"Inside Rust" blog](https://blog.rust-lang.org/inside-rust/). Here we will walk
through the process of creating such a second blog.

For the purposes of this doc, we will create an blog called "Outside Spade"
where we write a post every time we touch grass.

## Create a landing page

The landing page contains a short introduction to the blog followed by a list of
all posts ordered by date.

Start by creating a directory for this blog.

```shell
$ mkdir content/outside-spade
```

The path won't necessarily be the same as the final path in the URL but it's
easier to navigate if they're the same. TODO check this

Next, the content of the landing page is written in the file
`content/outside-spade/_index.md`:

```markdown
-< In content/outside-spade/_index.md >-
+++
title = "Outside Spade"
sort_by = "date"
+++

This is the "Outside Spade" blog where we try to touch grass.

You might be looking for [the normal Spade blog](/).
```

This will be rendered using the `section.html`-template.

You can now `zola serve` and check out the new blog at `/outside-spade`.

## Create a blog post

With our landing page done we can write our first blog post. Create and edit
your post, e.g. `content/outside-spade/2022-08-15-first-time.md`:

```markdown
-< In your post, e.g. content/outside-spade/2022-08-15-first-time.md >-
+++
title = "I touched grass"
-< "Date" isn't needed since it's in the file name >-
[extra]
author = "Ferris"
+++

This is the story of a crab

Who went outside and found some grass to grab

And while she looks so sad in photographs

I absolutely love her

When she smiles
```

`zola serve` again and you should be able to see your post both in the list at
`/outside-spade` and published as `/outside-spade/first-time`. Note that the
top header points to our new blog and not the main one.

## Left to the reader as an exercise

In `config.toml` you can add a navbar entry in `extra.menu` with a relative link
to `/outside-spade` and/or the main blog at `/`. Or maybe you'd prefer a
link to "All blogs" at `/blogs`?
