+++
title = "Spade 0.2.0"
[extra]
author = "Frans Skarman"
+++

<div id="release-links">
<a href="https://gitlab.com/spade-lang/spade/-/blob/master/CHANGELOG.md#020-2023-03-07">Associated changelog</a>
<a href="https://gitlab.com/spade-lang/spade/-/tree/v0.2.0">Git</a>
<a href="https://doi.org/10.5281/zenodo.7729341">DOI</a>
</div>

6 weeks have passed, so it is time for another Spade release! A lot of my time
has been spent at or preparing for conferences, so there are no huge changes
this time, but quite a few convenience features.

## Memory Initial Content

The standard library has been extended with another function:
`std::mem::clocked_memory_init`. Like its sibling `clocked_memory`, it defines
a memory which can be read by `read_memory`, but this one also allows
specifying initial content of the memory.

This can be used to build ROMs if no write ports are specified, or RAMs with
initial content if the available memories support it.

```spade
let rom = clocked_memory_init$(
    clk,
    writes: [],
    initial: [1,2,3,4]
);
reg(clk) rom_out = read_mem(rom, addr);
```

## More arithmetic ops

This release adds `min`, `max` and `order` functions to the standard library
under the `std::ops` namespace, and the `!=` operator to the core language.

## (Hacky) literals

An issue that has been plaguing the language for a while is a lack of support for large or negative integer literals. For example, the following code failed to compile
```spade
let x: int<8> = -128
```
This is clearly not intentional, and has been fixed.

In a related issue, the following code also failed to compile
```spade
let x: int<8> = 128
```
While this _is_ incorrect code since `128` does not fit in an 8-bit integer, Spade does not currently have an unsigned type and some features such as array indices treat signed integers as unsigned. This means that code like
```spade
let array = [1,2,3,4];
let value = array[3];
```
fails to compile. Until Spade gets a proper unsigned type, you can now suffix integer literals with `u`, making the following code work
```spade
let array = [1,2,3,4];
let value = array[3u];
```

It is worth pointing out that this is only an integer literal representation change, so using u for anything but index-like operations will fail. For example,
```spade
let x: int<8> = 1;
let y: int<8> = 150u;
x > y
```
will evaluate to `false`.

## Standard Library in Stand-alone Projects

Previously, the standard library was included by `swim` when compiling
projects. But this meant that one had to manually include the standard library
when running the spade compiler in stand-alone mode without swim.

As of this version, `spade` now includes the standard library and prelude
itself, allowing its use without swim.

Note: If you use an old `swim` with a new `spade`, or vice versa, you will see
errors when building swim projects. Make sure to update both or neither.

## Error message improvements

After 4 years of resistance, my advisor has finally written some Rust! Among
other things, he made numerous improvements to the grammar and clarity of the
error messages coming out of the compiler.

## Publications

I managed to visit 2 workshops in the last month:
[Latte'23](https://capra.cs.cornell.edu/latte23/) and [OSDA](https://osda.ws/).
If you want to see the papers or presentations, check the publication list at
https://spade-lang.org/#publications
