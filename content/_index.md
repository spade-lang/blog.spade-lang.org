+++
sort_by = "date"
insert_anchor_links = "right"
template = "section.html"
[extra.show_posts]
enable = true
dates = true
authors = true
+++

Welcome to the Spade blog! If you don't know what Spade is you can read an
introduction at the [main website](https://spade-lang.org). This blog is used by
the Spade contributors to announce progress, showcases and other information.

There is also an [RSS feed](/atom.xml).
