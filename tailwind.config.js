/** @type {import('tailwindcss').Config} */

const { themeVariants, prefersLight } = require("tailwindcss-theme-variants");

module.exports = {
  content: ["./templates/**/*.html", "./themes/spade/templates/**/*.html"],
  darkMode: "class",
  /* Classes to include even if they're not found in the content. */
  safelist: [
    'dark:block',
    'dark:hidden',

    'block',
    'hidden',

    'h-16',
  ],
  /* Theme modifications */
  theme: {
    extend: {
      fontFamily: {
        sans: ['"Atkinson Hyperlegible"']
      },
      screens: {
        'nav': '1000px',
      },
      colors: {
        'space-gray': {
          '50': '#f4f6f7',
          '100': '#e3e9ea',
          '200': '#c9d3d8',
          '300': '#a4b4bc',
          '400': '#748b97',
          '500': '#5c727e',
          '600': '#4f606b',
          '700': '#44505a',
          '800': '#3d464d',
          '900': '#363c43',
          '950': '#21262b',
        },
      },
      /* Defaults: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js */
      typography: (theme) => ({
        DEFAULT: {
          css: {
            /* Remove backticks around inline code. */
            'code::before': {
              content: 'unset',
            },
            'code::after': {
              content: 'unset',
            },
          },
        },
        /* More text contrast.
         * TODO: Global style that can be used outside prose too.
         */
        neutral: {
          css: {
            '--tw-prose-body': theme.colors.neutral[900],
            '--tw-prose-headings': theme.colors.black,
            '--tw-prose-invert-body': theme.colors.neutral[200],
          },
        },
      }),
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography'),
    themeVariants({
      themes: {
        light: {
           mediaQuery: prefersLight /* "@media (prefers-color-scheme: light)" */,
        },
      }
    })
  ]
};
